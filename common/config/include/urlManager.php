<?php
return [
    'enablePrettyUrl'     => true,
    'enableStrictParsing' => true,
    'showScriptName'      => false,
    'rules'               => [
        ''                                                                 => '/cms/dashboard',
        '<_m:[\w\-]+>/<_c:[\w\-]+>/<id:\d+>/<title:.*?>.html'              => '<_m>/<_c>/view',
        '<_m:[\w\-]+>/<_c:[\w\-]+>.html'                                   => '<_m>/<_c>/index',
        '<_m:[\w\-]+>/<_c:[\w\-]+>/<_a:[\w\-]+>/<id:\d+>/<title:.*?>.html' => '<_m>/<_c>/<_a>',
        '<_m:[\w\-]+>/<_c:[\w\-]+>/<_a:[\w\-]+>.html'                      => '<_m>/<_c>/<_a>',
        //'<_m:[\w\-]+>/<_c:[\w\-]+>'                                        => '<_m>/<_c>/index',

        '/users/service/auth/<service>.html' => '/users/service/auth',
        '/sitemap.xml' => '/site/sitemap/index',
        '/media/file/cke-upload' => '/media/file/cke-upload',
    ],
];
