<?php
return [
    'translations' => [
        'variety'  => [
            'class'          => 'yii\i18n\PhpMessageSource',
            'basePath'       => '@vendor/emilasp/yii2-variety/messages',
            'sourceLanguage' => 'en_US',
            'fileMap'        => [
                'variety' => 'variety.php',
            ],
        ],
        'settings' => [
            'class'          => 'yii\i18n\PhpMessageSource',
            'basePath'       => '@vendor/emilasp/yii2-settings/messages',
            'sourceLanguage' => 'en_US',
            'fileMap'        => [
                'settings' => 'settings.php',
            ],
        ],
        'taxonomy' => [
            'class'          => 'yii\i18n\PhpMessageSource',
            'basePath'       => '@vendor/emilasp/yii2-taxonomy-x/messages',
            'sourceLanguage' => 'en_US',
            'fileMap'        => [
                'taxonomy' => 'taxonomy.php',
            ],
        ],
        'json'     => [
            'class'          => 'yii\i18n\PhpMessageSource',
            'basePath'       => '@vendor/emilasp/yii2-json/messages',
            'sourceLanguage' => 'en_US',
            'fileMap'        => [
                'json' => 'json.php',
            ],
        ],
        'unidoc' => [
            'class'          => 'yii\i18n\PhpMessageSource',
            'basePath'       => '@vendor/emilasp/yii2-unidoc/messages',
            'sourceLanguage' => 'en_US',
            'fileMap'        => [
                'unidoc' => 'unidoc.php',
            ],
        ],
        'library' => [
            'class'          => 'yii\i18n\PhpMessageSource',
            'basePath'       => '@vendor/emilasp/yii2-unidoc-library/messages',
            'sourceLanguage' => 'en_US',
            'fileMap'        => [
                'library' => 'library.php',
            ],
        ],
        'tasks' => [
            'class'          => 'yii\i18n\PhpMessageSource',
            'basePath'       => '@vendor/emilasp/yii2-unidoc-tasks/messages',
            'sourceLanguage' => 'en_US',
            'fileMap'        => [
                'tasks' => 'tasks.php',
            ],
        ],
        'notes' => [
            'class'          => 'yii\i18n\PhpMessageSource',
            'basePath'       => '@vendor/emilasp/yii2-unidoc-notes/messages',
            'sourceLanguage' => 'en_US',
            'fileMap'        => [
                'notes' => 'notes.php',
            ],
        ],
        'users'   => [
            'class'          => 'yii\i18n\PhpMessageSource',
            'basePath'       => '@vendor/emilasp/yii2-user-x/messages',
            'sourceLanguage' => 'en_US',
            'fileMap'        => [
                'users' => 'users.php',
            ],
        ],

        'site'   => [
            'class'          => 'yii\i18n\PhpMessageSource',
            'basePath'       => '@vendor/emilasp/yii2-site-x/messages',
            'sourceLanguage' => 'en_US',
            'fileMap'        => [
                'site' => 'site.php',
            ],
        ],
        'cms'   => [
            'class'          => 'yii\i18n\PhpMessageSource',
            'basePath'       => '@vendor/emilasp/yii2-cms/messages',
            'sourceLanguage' => 'en_US',
            'fileMap'        => [
                'cms' => 'cms.php',
            ],
        ],
        'social'   => [
            'class'          => 'yii\i18n\PhpMessageSource',
            'basePath'       => '@vendor/emilasp/yii2-social/messages',
            'sourceLanguage' => 'en_US',
            'fileMap'        => [
                'social' => 'social.php',
            ],
        ],

        'media'   => [
            'class'          => 'yii\i18n\PhpMessageSource',
            'basePath'       => '@vendor/emilasp/yii2-media/messages',
            'sourceLanguage' => 'en_US',
            'fileMap'        => [
                'media' => 'media.php',
            ],
        ],
    ],
];
