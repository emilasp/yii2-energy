<?php

use emilasp\media\models\File;
use emilasp\publisher\SocialPublisherModule;
use emilasp\users\common\models\Profile;
use emilasp\users\backend\rbac\rules\UserGroupRule;

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'modules'    => [
        'variety' => [
            'class' => 'emilasp\variety\VarietyModule',
        ],
        'json'    => [
            'class' => 'emilasp\json\JsonModule',
        ],

        'settings' => [
            'class' => 'emilasp\settings\SettingModule',
        ],

        'media' => [
            'class'     => 'emilasp\media\MediaModule',
            'pathCache' => '@backend/web/media/files',
            'config'    => [
                'watermarkSrc'   => '@backend/web/images/watermark.png',
                'scaleWatermark' => 5,
                'quality'        => 100,
                'noImage'        => [
                    'path' => '/media/system/noImage/',
                    'file' => 'no-image.png',
                ],
            ],
            'sizes'     => [
                'default'            => [
                    File::SIZE_ICO => ['size' => '80x80', 'watermark' => false, 'crop' => true],
                    File::SIZE_MIN => ['size' => '120x120', 'watermark' => true, 'aspectRatio' => true, 'crop' => true],
                    File::SIZE_MID => ['size' => '250x250', 'watermark' => true, 'aspectRatio' => true, 'crop' => true],
                    File::SIZE_MED => ['size' => '400x', 'watermark' => true, 'aspectRatio' => true],
                    File::SIZE_MAX => ['size' => '900x', 'watermark' => true, 'aspectRatio' => true],
                    File::SIZE_ORG => ['watermark' => false, 'ratio' => true],
                ],
                Profile::class => [
                    File::SIZE_ICO => ['size' => '100x100', 'watermark' => false, 'crop' => true],
                    File::SIZE_MIN => ['size' => '96x144', 'watermark' => true, 'aspectRatio' => true],
                    File::SIZE_MID => ['size' => '218x320', 'watermark' => true, 'aspectRatio' => true],
                    File::SIZE_MED => ['size' => '400x600', 'watermark' => true, 'aspectRatio' => true],
                    File::SIZE_MAX => ['size' => '800x600', 'watermark' => true, 'aspectRatio' => true],
                    File::SIZE_ORG => ['watermark' => false, 'ratio' => true],
                ],
            ],
        ],
        'publisher' => [
            'class' => SocialPublisherModule::class
        ]
    ],

    'components' => [
        'db'     => require(__DIR__ . '/include/db.php'),
        'redis'  => require(__DIR__ . '/include/redis.php'),
        'sphinx' => require(__DIR__ . '/include/sphinx.php'),
        'mailer' => require(__DIR__ . '/include/mail.php'),

        'cache' => [
            'class' => 'yii\redis\Cache',
            'redis' => [
                'hostname' => 'localhost',
                'port'     => 6379,
                'database' => 2,
            ],
        ],

        'authManager' => [
            'class'          => 'yii\rbac\PhpManager', // or use 'yii\rbac\DbManager'
            'defaultRoles'   => [
                UserGroupRule::ROLE_USER,
                UserGroupRule::ROLE_AUTHOR,
                UserGroupRule::ROLE_AUDITOR,
                UserGroupRule::ROLE_ADMIN,
            ],
            'itemFile'       => '@common/config/rbac/items.php',
            'assignmentFile' => '@common/config/rbac/assignments.php',
            'ruleFile'       => '@common/config/rbac/rules.php'
        ],

        'i18n'       => require(__DIR__ . '/include/i18n.php'),
        //'urlManager' => require(__DIR__ . '/include/urlManager.php'),

        'formatter' => [
            'class' => 'yii\i18n\Formatter',
        ],
    ],
];
