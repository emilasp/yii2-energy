<?php
return [
    'adminEmail'                    => 'admin@example.com',
    'supportEmail'                  => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,

    'email' => [
        'admin'   => 'law@pravo-zakonno.ru',
        'manager' => 'fremil@yandex.ru',
    ],

    //'bsVersion' => '4.x',
];
