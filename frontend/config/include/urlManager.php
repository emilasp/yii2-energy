<?php
return [
    'baseUrl' => 'https://www.pravo-zakonno.ru/',

    'enablePrettyUrl'     => true,
    'enableStrictParsing' => true,
    'showScriptName'      => false,
    'rules'               => [

        '' => '/site/site/index',

        '/tags.html'                   => '/cms/tag/index',
        '/tag/<title:[\w\d\_-]+>.html' => '/cms/content/tag',

        '/categories.html'                  => '/cms/category/index',
        '/category/<title:[\w\d\_-]+>.html' => '/cms/content/category',

        '<title:[\w\d\_-]+>.html' => '/cms/content/view',

        '/search/<search:[\w\d\ _-]+>.html' => '/cms/search/index',

        '/social/rating/rate.html' => '/social/rating/rate',

        //'/page/<title:[\w\d\_-]+>.html' => '/site/page/view-by-title',

        '/users/service/auth/<service>.html' => '/users/service/auth',
        '/sitemap.xml'                       => '/site/sitemap/index',

    ],
];
