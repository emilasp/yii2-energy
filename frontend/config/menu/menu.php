<?php
$username = !Yii::$app->user->isGuest ? Yii::$app->user->identity->username : '';

return [
    [
        'label'  => '<i class="fa fa-book"></i> ' . Yii::t('library', 'Library'),
        'url'    => '#',
        'active' => false,
        'role'   => '@',
        'items'  => [
            [
                'label'  => Yii::t('library', 'Library Learns'),
                'url'    => '#',
                'active' => true,
                'icon'   => 'fa fa-graduation-cap',
                'role'   => '@',
                #'role'   => ['admin', 'manager'],
                'items'  => [
                    ['label' => 'Список', 'url' => '/library/learn/index', 'role' => ['@']],
                    ['label' => 'Добавить', 'url' => '/library/learn/create', 'role' => ['@']],
                ],
            ],
            [
                'label'  => Yii::t('media', 'Files'),
                'url'    => '#',
                'active' => true,
                'icon'   => 'fa fa-picture-o',
                #'role'   => ['admin', 'manager'],
                'items'  => [
                    ['label' => 'Список', 'url' => '/media/file/index', 'role' => ['@']],
                    ['label' => 'Добавить', 'url' => '/media/file/create', 'role' => ['@']],
                ],
            ],
        ],
    ],
    [
        'label'  => '<i class="fa fa-book"></i> ' . Yii::t('tasks', 'Tasks'),
        'url'    => '#',
        'active' => false,
        'role'   => '@',
        'items'  => [
            [
                'label'  => Yii::t('tasks', 'Strategies'),
                'url'    => '#',
                'active' => true,
                'icon'   => 'fa fa-graduation-cap',
                'role'   => '@',
                #'role'   => ['admin', 'manager'],
                'items'  => [
                    ['label' => 'Список', 'url' => '/tasks/strategy/index', 'role' => ['@']],
                    ['label' => 'Добавить', 'url' => '/tasks/strategy/create', 'role' => ['@']],
                ],
            ],
            [
                'label'  => Yii::t('tasks', 'Projects'),
                'url'    => '#',
                'active' => true,
                'icon'   => 'fa fa-picture-o',
                'role'   => '@',
                #'role'   => ['admin', 'manager'],
                'items'  => [
                    ['label' => 'Список', 'url' => '/tasks/project/index', 'role' => ['@']],
                    ['label' => 'Добавить', 'url' => '/tasks/project/create', 'role' => ['@']],
                ],
            ],
            [
                'label'  => Yii::t('tasks', 'Tasks'),
                'url'    => '#',
                'active' => true,
                'icon'   => 'fa fa-picture-o',
                'role'   => '@',
                #'role'   => ['admin', 'manager'],
                'items'  => [
                    ['label' => 'Список', 'url' => '/tasks/task/index', 'role' => ['@']],
                    ['label' => 'Добавить', 'url' => '/tasks/task/create', 'role' => ['@']],
                ],
            ],
        ],
    ],

    [
        'label'  => '<i class="fa fa-book"></i> ' . Yii::t('unidoc', 'Documents'),
        'url'    => '#',
        'active' => false,
        'role'   => '@',
        'items'  => [
            [
                'label'  => Yii::t('unidoc', 'Strategies'),
                'url'    => '#',
                'active' => true,
                'icon'   => 'fa fa-graduation-cap',
                'role'   => 'admin',
                'items'  => [
                    ['label' => 'Список', 'url' => '/unidoc/strategy/index', 'role' => ['@']],
                    ['label' => 'Добавить', 'url' => '/unidoc/strategy/create', 'role' => ['@']],
                ],
            ],
            [
                'label'  => Yii::t('unidoc', 'Projects'),
                'url'    => '#',
                'active' => true,
                'icon'   => 'fa fa-picture-o',
                'role'   => 'admin',
                'items'  => [
                    ['label' => 'Список', 'url' => '/unidoc/project/index', 'role' => ['@']],
                    ['label' => 'Добавить', 'url' => '/unidoc/project/create', 'role' => ['@']],
                ],
            ],
            [
                'label'  => Yii::t('unidoc', 'Documents'),
                'url'    => '#',
                'active' => true,
                'icon'   => 'fa fa-picture-o',
                'role'   => 'admin',
                'items'  => [
                    ['label' => 'Список', 'url' => '/unidoc/task/index', 'role' => ['@']],
                    ['label' => 'Добавить', 'url' => '/unidoc/task/create', 'role' => ['@']],
                ],
            ],
        ],
    ],
    [
        'label'  => Yii::t('notes', 'Notes'),
        'url'    => '#',
        'active' => true,
        'icon'   => 'fa fa-picture-o',
        'role'   => 'admin',
        'items'  => [
            ['label' => 'Список', 'url' => '/notes/dashboard/index', 'role' => ['@']],
            ['label' => 'Добавить', 'url' => '/notes/note/create', 'role' => ['@']],
        ],
    ],
    [
        'label'  => '<i class="fa fa-address-card"></i> ' . Yii::t('users', 'Users'),
        'url'    => '#',
        'active' => false,
        'items'  => [
            [
                'label'  => Yii::t('users', 'Management'),
                'url'    => '#',
                'active' => true,
                'icon'   => 'fa fa-bars',
                #'role'   => ['admin', 'manager'],
                'items'  => [
                    ['label' => 'Список', 'url' => '/users/manage/index', 'role' => ['@']],
                    ['label' => 'Добавить', 'url' => '/users/manage/create', 'role' => ['@']],
                ],
            ],
        ],
    ],
    [
        'label'  => Yii::t('site', 'Content'),
        'url'    => '#',
        'icon'   => 'fa fa-edit',
        'active' => false,
        'items'  => [
            [
                'label' => Yii::t('site', 'Pages'),
                'url'   => '#',
                'icon'  => 'fa fa-map-o',
                'role'  => ['admin'],
                'items' => [
                    ['label' => 'Список', 'url' => '/site/page/index', 'role' => ['@']],
                    ['label' => 'Добавить', 'url' => '/site/page/create', 'role' => ['@']],
                ],
            ],
            [
                'label' => Yii::t('site', 'Files'),
                'url'   => '#',
                'icon'  => 'fa fa-folder',
                'role'  => ['admin'],
                'items' => [
                    ['label' => 'Список', 'url' => '/files/file/index', 'role' => ['@']],
                    ['label' => 'Добавить', 'url' => '/files/file/create', 'role' => ['@']],
                ],
            ],
        ],
    ],
    [
        'label'   => Yii::t('users', 'Login'),
        'url'     => '/users/user/login',
        'icon'    => 'fa fa-key',
        'visible' => Yii::$app->user->isGuest
    ],
    [
        'label'       => Yii::t('users', 'Logout') . ' <small>(' . $username . ')</small>',
        'url'         => '/users/user/logout',
        'icon'        => 'fa fa-key',
        'linkOptions' => ['data-method' => 'POST'],
        'role'        => ['@']
    ],
];
