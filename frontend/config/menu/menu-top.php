<?php

use emilasp\cms\common\models\ContentCategory;

/*
$otherItems = ContentCategory::find()->where(['tree' => 1, 'depth' => 2])->orderBy('name')->all();

$baseItems[] = [
    'label'  => 'Другое',
    'url'    => '#',
    'active' => false,
    'role'   => '@',
    'items'  => array_map(function($model) {
        return ['label' => $model->name, 'url' => $model->getUrl()];
    }, $otherItems)
];*/


return [


    [
        'label' => 'Семейное право',
        'url'   => '/categories.html',
    ],

    [
        'label' => 'Категории',
        'url'   => '/categories.html',
    ],
    [
        'label' => 'Калькуляторы',
        'url'   => '/tags.html',
    ],

    [
        'label'  => 'О нас',
        'url'    => '#',
        'active' => false,
        'role'   => '@',
        'icon'   => 'fa fa-check text-info',
        'items'  => [
            [
                'label'   => 'О проекте',
                'urlType' => true,
                'url'     => '/o-proekte.html',
                'icon'    => 'fa fa-compass text-info',
            ],
            [
                'label'   => 'Политика конфеденциальности',
                'urlType' => true,
                'url'     => '/politika-bezopastnosti.html',
                'icon'    => 'fa fa-check-circle text-info',
            ],
            [
                'label'   => 'Контакты',
                'urlType' => true,
                'url'     => '/kontakty.html',
                'icon'    => 'fa fa-address-book text-info',
            ],
        ]
    ],
];
