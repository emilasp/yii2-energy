<?php
use kartik\nav\NavX;
use yii\bootstrap\NavBar;
use yii\helpers\Html;

$navBarOptions = [
    'options'               => [
        'class' => 'navbar navbar-inverse navbar-fixed-top',
        'id'    => 'topMenu',
    ],
    'innerContainerOptions' => ['class' => ''],
];


NavBar::begin($navBarOptions);

echo Html::beginTag('div', ['class' => 'container']);

echo ' <a class="navbar-brand" href="/">
        <img alt="Brand" src="/images/logo.png">
      </a>';


foreach ($menus as $menu) {
    $_menu = [
        'activateParents' => true,
        'encodeLabels'    => false,
    ];

    if (isset($menu['options'])) {
        $_menu['options'] = $menu['options'];
    }
    if (isset($menu['items'])) {
        $_menu['items'] = $menu['items'];
    }

    //@TODO
    echo NavX::widget($_menu);
}


if (!empty($menusRight)) {
    foreach ($menusRight as $menu) {
        $_menu = [
            'activateParents' => true,
            'encodeLabels'    => false,
        ];

        if (isset($menu['options'])) {
            $_menu['options'] = $menu['options'];
        }
        if (isset($menu['items'])) {
            $_menu['items'] = $menu['items'];
        }

        //@TODO
        echo NavX::widget($_menu);
    }
}

echo Html::endTag('div');

NavBar::end();
