<?php
/* @var $this \yii\web\View */

/* @var $content string */

use emilasp\cms\frontend\widgets\SearchWidget\SearchWidget;
use emilasp\core\assets\ThemeBaseAsset;
use emilasp\cms\common\models\Article;
use emilasp\cms\common\models\ContentCategory;
use emilasp\cms\frontend\widgets\CategoryWidget\CategoryWidget;
use emilasp\cms\frontend\widgets\ContentSidebarWidget\ContentSidebarWidget;
use emilasp\cms\frontend\widgets\TagWidget\TagWidget;
use emilasp\core\extensions\AjaxProgressLine\AjaxProgressLine;
use emilasp\site\frontend\assets\AppAsset;
use emilasp\site\frontend\widgets\MenuHorizontal\MenuHorizontal;
use emilasp\users\frontend\widgets\LoginWidget\LoginWidget;
use frontend\themes\energy\ThemeAsset;
use yii\helpers\Html;
use emilasp\site\common\extensions\FlashMsg\FlashMsg;
use emilasp\site\common\extensions\breadcrumbs\Breadcrumbs;

?>

<?php AppAsset::register($this); ?>
<?php ThemeAsset::register($this); ?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" itemscope itemtype="http://schema.org/WebPage">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?= Html::csrfMetaTags() ?>

    <title itemprop="name"><?= Html::encode(strip_tags($this->title)) ?></title>

    <?php $this->head() ?>
</head>

<body>
<?php $this->beginBody() ?>

<?php FlashMsg::widget(); ?>

<header>

    <div class="container">
        <div class="row">
            <div class="col-xl-12 headerBlock">

                <div class="headerInner" itemscope itemtype="http://schema.org/Organization">
                    <div class="logo">
                        <a href="/">
                            <img src="/images/logo.png" alt="logo" itemprop="logo">
                        </a>
                    </div>
                    <div class="headerText">
                        <p class="headerText1"><?= Yii::$app->getModule('site')->getSetting('site_name') ?></p>
                        <p class="headerText2"><?= Yii::$app->getModule('site')->getSetting('site_description') ?></p>
                    </div>
                    <div class="headerLibra">
                        <img src="/images/libra.jpg" alt="">
                    </div>
                    <div class="headerBtn">
                        <a href="#">
                            <i class="fas fa-question"></i>
                            <span>Задать вопрос</span>
                        </a>
                    </div>
                    <div class="headerContacts">
                        <div class="hcPhone">
                            <a href="tel:<?= Yii::$app->getModule('site')
                                ->getSetting('site_phone', true, true, function ($data) {
                                    return str_replace(
                                        '-',
                                        '',
                                        filter_var(strip_tags($data), FILTER_SANITIZE_NUMBER_INT)
                                    );
                                }) ?>">
                                <i class="fas fa-phone"></i>
                                <span class="phPhoneN1" itemprop="telephone">
                                    <?= Yii::$app->getModule('site')->getSetting('site_phone') ?>
								</span>
                            </a>
                        </div>
                        <div class="hcMail">
                            <a href="mailto:<?= Yii::$app->getModule('site')->getSetting('site_email') ?>"
                               target="_blank">
                                <i class="fas fa-envelope"></i>
                                <span itemprop="email">
                                    <?= Yii::$app->getModule('site')->getSetting('site_email') ?>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <nav class="mainMenu">
        <div class="container">

            <div class="row">
                <div class="col-lg-10">
                    <?= MenuHorizontal::widget(['template' => 'menu-bs4-2']) ?>
                </div>
                <div class="col-lg-2">
                    <?= SearchWidget::widget() ?>
                </div>

            </div>

        </div>
    </nav>

</header>

<div class="container">

    <div class="row mainContent">
        <main class="col-xl-12">

            <?= Breadcrumbs::widget(['items' => $this->params['breadcrumbs']]) ?>

            <?= AjaxProgressLine::widget() ?>

            <?= $content ?>

        </main>

    </div>
</div>


<footer class="footerTop">

    <div class="container">
        <div class="row">
            <div class="col-xl-12">

                <div class="row">
                    <div class="col-xl-3 footerLogo">
                        <a href="#">
                            <img src="/images/logo.png" alt="">
                        </a>
                    </div>
                    <div class="col-xl-3">
                        <div class="footerTextBlock">
                            <header class="footerHeader">
                                <?= Yii::$app->getModule('site')->getSetting('site_description') ?>
                            </header>
                        </div>
                    </div>
                    <div class="col-xl-3">
                        <div class="footerUlBlock">
                            <header class="footerHeader">
                                О нас
                            </header>
                            <nav>
                                <ul itemscope itemtype="http://www.schema.org/SiteNavigationElement">
                                    <li itemprop="name">
                                        <a itemprop="url" href="/o-proekte.html">О проекте</a>
                                    </li>
                                    <li itemprop="name">
                                        <a itemprop="url" href="/kontakty.html">Контакты</a>
                                    </li>
                                    <li itemprop="name">
                                        <a itemprop="url" href="/politika-bezopastnosti.html">Политика безопастности</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="col-xl-3">
                        <div class="footerSocBlock">
                            <header class="footerHeader">
                                Мы в соцсетях
                            </header>
                            <div class="footerSocBlockIn">
                                <!--<a href="#"><img src="images/ok.jpg" alt=""></a>
                                <a href="#"><img src="images/vk.jpg" alt=""></a>
                                <a href="#"><img src="images/fb.jpg" alt=""></a>
                                <a href="#"><img src="images/gp.jpg" alt=""></a>
                                <a href="#"><img src="images/tw.jpg" alt=""></a>-->
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>


    <div class="footerBottom">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="footerBottomInner">
                            <div class="footerBottomT1">
                                Copyright © 2016-<?= date('Y') ?>
                                «<?= Yii::$app->getModule('site')->getSetting('site_name') ?>»
                            </div>
                            <div class="footerBottomT2">Копирование материалов сайта запрещено!</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</footer>


<?= LoginWidget::widget() ?>

<?php $this->endBodyPreEnd() ?>

<?php $this->endBody() ?>


</body>
</html>
<?php $this->endPage() ?>
