<?php

namespace frontend\themes\energy;

use emilasp\core\components\base\AssetBundle;
use Yii;

/**
 * Class ThemeAsset
 * @package frontend\themes\energy
 */
class ThemeAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';
    public $css        = [
        'css/mobile',
        'css/header',
        'css/menu',
        'css/content',
        'css/aside',
        'css/articles',
        'css/article',
        'css/footer',
        'css/comments',
        'css/categories',
        'css/custom',
        'fonts/stylesheet',
    ];
}
