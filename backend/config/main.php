<?php

use emilasp\cms\backend\CmsModule;
use emilasp\core\components\base\View;
use emilasp\site\backend\SiteModule;
use emilasp\social\backend\SocialModule;

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id'                  => 'app-backend',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'emilasp\site\controllers',
    'defaultRoute'        => '/site/page/index',
    'language'            => 'ru_RU',
    'sourceLanguage'      => 'en_US',

    'modules'    => [
        'core' => [
            'class'             => 'emilasp\core\CoreModule',
            'enableTheme'       => true,
            'enableModuleTheme' => true,
            'theme'             => 'materialized',
        ],

        'users' => [
            'class'           => 'emilasp\users\backend\UsersModule',
            'routeAfterLogin' => '/'
        ],

        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],

        'taxonomy' => [
            'class' => 'emilasp\taxonomy\TaxonomyModule',
        ],

        'seo' => [
            'class' => 'emilasp\seo\SeoModule',
        ],

        'site'   => [
            'class' => 'emilasp\site\backend\SiteModule',
        ],
        'cms'    => [
            'class' => 'emilasp\cms\backend\CmsModule',
        ],
        'social' => [
            'class' => 'emilasp\social\backend\SocialModule',
        ],

        'settings'   => [
            'class'       => 'emilasp\settings\SettingModule',
            'onAdminPage' => [
                SiteModule::className(),
                CmsModule::className(),
                SocialModule::className(),
            ],
        ],
        'commission' => ['class' => 'emilasp\commission\backend\CommissionModule'],
    ],
    'components' => [
        'request'  => [
            'enableCookieValidation' => true,
            'enableCsrfValidation'   => true,
            'cookieValidationKey'    => 'werwer56yhujm,86756weavmf5017',
        ],
        'stockApi' => [
            'class'  => \emilasp\media\components\stockApi\ShutterstockApi::class,
            'key'    => '',
            'secret' => '',
        ],
        'user'     => [
            'class'           => 'emilasp\users\common\components\UserComponent',
            'identityClass'   => 'emilasp\users\common\models\User',
            'enableAutoLogin' => true,
            'authTimeout'     => 8640000,
            'identityCookie'  => ['name' => '_identity-backend', 'httpOnly' => true],
            'loginUrl'        => ['/users/user/login']
        ],
        'access'   => [
            'class' => 'yii\web\AccessControl',
        ],
        'session'  => [
            'name' => 'advanced-backend',
        ],

        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => '/site/site/error',
        ],


        'assetManager' => [
            'linkAssets' => YII_DEBUG,
        ],
        'view'         => [
            'class'        => 'emilasp\core\components\base\View',
            'enableMinify' => false,
            'theme'        => [
                'pathMap' => [
                    '@app/views' => '@app/themes/materialized/views',
                ],
            ],
        ],

        'urlManager' => [
            'enablePrettyUrl'     => true,
            'enableStrictParsing' => true,
            'showScriptName'      => false,
            'rules'               => [
                ''                                                                 => '/cms/dashboard',
                '<_m:[\w\-]+>/<_c:[\w\-]+>/<id:\d+>/<title:.*?>.html'              => '<_m>/<_c>/view',
                '<_m:[\w\-]+>/<_c:[\w\-]+>.html'                                   => '<_m>/<_c>/index',
                '<_m:[\w\-]+>/<_c:[\w\-]+>/<_a:[\w\-]+>/<id:\d+>/<title:.*?>.html' => '<_m>/<_c>/<_a>',

                '<title:[\w\d\_-]+>.html' => '/cms/content/view',

                '<_m:[\w\-]+>/<_c:[\w\-]+>/<_a:[\w\-]+>.html' => '<_m>/<_c>/<_a>',
                //'<_m:[\w\-]+>/<_c:[\w\-]+>'                                        => '<_m>/<_c>/index',

            ],
        ],
    ],
    'params'     => $params,
];
