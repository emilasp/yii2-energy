<?php
$username = !Yii::$app->user->isGuest ? Yii::$app->user->identity->username : '';

return [
    [
        'label'  => Yii::t('cms', 'Content'),
        'url'    => '#',
        'icon'   => 'fa fa-list-alt',
        'active' => false,
        'role'   => '@',
        'items'  => [
            [
                'label'  => Yii::t('cms', 'Articles'),
                'url'    => '/cms/article/index',
                'active' => false,
                'role'   => '@',
            ],
            [
                'label'  => Yii::t('cms', 'News'),
                'url'    => '/cms/news/index',
                'active' => false,
                'role'   => '@',
            ],
            '<li class="divider"></li>',
            [
                'label'  => Yii::t('cms', 'Content Tasks'),
                'url'    => '/cms/content-task/index',
                'active' => false,
                'role'   => 'admin',
            ],
            '<li class="divider"></li>',
            [
                'label'  => Yii::t('social', 'Comments'),
                'url'    => '/social/comment/index',
                'active' => false,
                'role'   => 'admin',
            ],
            '<li class="divider"></li>',
            [
                'label'  => Yii::t('site', 'Pages'),
                'url'    => '/site/page/index',
                'active' => false,
                'role'   => '@',
            ],
        ]
    ],

    [
        'label'  => Yii::t('site', 'Add'),
        'url'    => '#',
        'icon'   => 'fa fa-plus',
        'active' => false,
        'role'   => '@',
        'items'  => [
            [
                'label'  => Yii::t('cms', 'Add Article'),
                'url'    => '/cms/article/create',
                'icon'   => 'fa fa-graduation-cap',
                'active' => false,
                'role'   => '@',
            ],
            [
                'label'  => Yii::t('cms', 'Add News'),
                'url'    => '/tasks/news/create',
                'icon'   => 'fa fa-address-card',
                'active' => false,
                'role'   => '@',
            ],
            '<li class="divider"></li>',
            [
                'label'  => Yii::t('site', 'Add Page'),
                'url'    => '/site/page/create',
                'icon'   => 'fa fa-road',
                'active' => false,
                'role'   => '@',
            ],
            '<li class="divider"></li>',
            [
                'label'  => Yii::t('cms', 'Add Content Task'),
                'url'    => '/cms/content-task/create',
                'icon'   => 'fa fa-bolt',
                'active' => false,
                'role'   => 'auditor',
            ],
        ]
    ],
];
