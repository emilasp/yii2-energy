<?php

use emilasp\cms\common\models\ContentCategory;
use yii\helpers\Url;

$username = !Yii::$app->user->isGuest ? Yii::$app->user->identity->username : '';

return [

    [
        'label'  => Yii::t('cms', 'Content'),
        'url'    => '#',
        'active' => false,
        'role'   => '@',
        'icon'   => 'fa fa-list-alt',
        'items'  => [
            [
                'label'  => Yii::t('cms', 'Articles'),
                'url'    => '/cms/article/index',
                'active' => false,
                'role'   => '@',
            ],
            [
                'label'  => Yii::t('cms', 'News'),
                'url'    => '/cms/news/index',
                'active' => false,
                'role'   => '@',
            ],
            [
                'label'  => Yii::t('site', 'Pages'),
                'url'    => '/site/page/index',
                'active' => false,
                'role'   => '@',
            ],
            [
                'label'  => Yii::t('cms', 'Content Tasks'),
                'url'    => '/cms/content-task/index-dashboard',
                'active' => false,
                'role'   => '@',
            ],
            [
                'label'  => Yii::t('cms', 'Semantic Kernels'),
                'url'    => '/cms/semantic-kernel/index',
                'active' => false,
                'role'   => '@',
            ],
        ]
    ],

    [
        'label'  => Yii::t('social', 'Social'),
        'url'    => '#',
        'active' => false,
        'role'   => '@',
        'icon'   => 'fa fa-list-alt',
        'items'  => [
            [
                'label'  => Yii::t('social', 'Comments'),
                'url'    => '/social/comment/index',
                'active' => false,
                'role'   => '@',
            ],
            [
                'label'  => Yii::t('social', 'Rates'),
                'url'    => '/social/rating/index',
                'active' => false,
                'role'   => '@',
            ],
        ]
    ],

    [
        'label'  => Yii::t('taxonomy', 'Taxonomies'),
        'url'    => '#',
        'active' => false,
        'role'   => '@',
        'icon'   => 'fa fa-list-alt',
        'items'  => [
            [
                'label'  => Yii::t('taxonomy', 'Categories'),
                'url'    => '/cms/content-category/index',
                'active' => false,
                'role'   => '@',
            ],
            [
                'label'  => Yii::t('taxonomy', 'Tags'),
                'url'    =>'/cms/content-tag/index',
                'active' => false,
                'role'   => '@',
            ],
        ]
    ],

    [
        'label' => Yii::t('media', 'Files'),
        'url'   => '#',
        'icon'  => 'fa fa-folder',
        'role'  => ['admin'],
        'items' => [
            ['label' => 'Список', 'url' => '/files/file/index', 'role' => ['@']],
            ['label' => 'Добавить', 'url' => '/files/file/create', 'role' => ['@']],
        ],
    ],

    [
        'label'  => Yii::t('users', 'Users'),
        'url'    => '#',
        'active' => false,
        'icon'   => 'fa fa-address-card',
        'items'  => [
            [
                'label'  => Yii::t('users', 'Management'),
                'url'    => '/users/manage/index',
                'active' => true,
                'icon'   => 'fa fa-bars',
                'role'   => ['admin', 'manager'],
            ],
        ],
    ],

    [
        'label'  => Yii::t('site', 'References'),
        'url'    => '#',
        'active' => false,
        'role'   => 'admin',
        'icon'   => 'fa fa-list-ol ',
        'items'  => [
            [
                'label'  => Yii::t('site', 'SEO'),
                'url'    => '/seo/seo-data/index',
                'active' => true,
                'icon'   => 'fa fa-graduation-cap',
                'role'   => ['admin'],
            ],
        ],
    ],
    [
        'label'  => Yii::t('site', 'Commissions'),
        'url'    => '#',
        'active' => false,
        'role'   => 'admin',
        'icon'   => 'fa fa-list-ol ',
        'items'  => [
            [
                'label'  => Yii::t('site', 'Каталог'),
                'url'    => '/commission/catalog/index',
                'active' => true,
                'icon'   => 'fa fa-graduation-cap',
                'role'   => ['admin'],
            ],
            [
                'label'  => Yii::t('site', 'Заказы'),
                'url'    => '/commission/order/index',
                'active' => true,
                'icon'   => 'fa fa-shopping-cart',
                'role'   => ['admin'],
            ],
            [
                'label'  => Yii::t('site', 'Планирование закупки'),
                'url'    => '/commission/reserve/index',
                'active' => true,
                'icon'   => 'fa fa-chart-line',
                'role'   => ['admin'],
            ],
            [
                'label'  => Yii::t('site', 'Статистика по годам'),
                'url'    => '/commission/reserve/years',
                'active' => true,
                'icon'   => 'fa fa-chart-line',
                'role'   => ['admin'],
            ],
            [
                'label'  => Yii::t('site', 'Анализ продаж/цен'),
                'url'    => '/commission/order-analize.html',
                'active' => true,
                'icon'   => 'fa fa-chart-line',
                'role'   => ['admin'],
            ],
            [
                'label'  => Yii::t('site', 'Сверка цен'),
                'url'    => '/commission/check-price/index',
                'active' => true,
                'icon'   => 'fa fa-eye',
                'role'   => ['admin'],
            ],
        ],
    ],

    [
        'label'   => Yii::t('settings', 'Settings'),
        'url'     => '/settings/default/index',
        'icon'    => 'fa fa-cogs',
        'visible' => Yii::$app->user->can('admin')
    ],

    [
        'label'   => Yii::t('users', 'Login'),
        'url'     => '/users/user/login',
        'icon'    => 'fa fa-key',
        'visible' => Yii::$app->user->isGuest
    ],
    [
        'label'       => Yii::t('users', 'Logout') . ' <small>(' . $username . ')</small>',
        'url'         => '/users/user/logout',
        'icon'        => 'fa fa-key',
        'linkOptions' => ['data-method' => 'POST'],
        'role'        => ['@']
    ],
];
