<?php
$username = !Yii::$app->user->isGuest ? Yii::$app->user->identity->username : '';

return [
    [
        'label'   => Yii::t('users', 'Login'),
        'url'     => '/users/user/login',
        'icon'    => 'fa fa-key',
        'visible' => Yii::$app->user->isGuest,
        'active'  => false,
    ],
    [
        'label'       => Yii::t('users', 'Logout') . ' <small>(' . $username . ')</small>',
        'url'         => '/users/user/logout',
        'icon'        => 'fa fa-key',
        'linkOptions' => ['data-method' => 'POST'],
        'visible'     => !Yii::$app->user->isGuest,
        'role'        => ['@']
    ],
];
