<?php

/* @var $this \yii\web\View */
/* @var $content string */

use emilasp\core\assets\ThemeBaseAsset;
use emilasp\core\extensions\AjaxProgressLine\AjaxProgressLine;
use emilasp\core\extensions\FlashMsg\FlashMsg;
use emilasp\site\common\extensions\breadcrumbs\Breadcrumbs;
use emilasp\site\common\extensions\menu\Menu;
use emilasp\site\common\extensions\MenuCustom\MenuCustom;
use emilasp\site\common\extensions\UserHistory\UserHistory;
use yii\helpers\Html;
use emilasp\admintheme\bundles\ThemeAsset;
use emilasp\admintheme\widgets\menu\MenuAdmin;
use emilasp\admintheme\widgets\userMenuAdmin\UserMenuAdmin;
use emilasp\admintheme\widgets\skinSwitcher\SkinSwitcherAdmin;
use yii\helpers\Url;

?>

<?php ThemeBaseAsset::register($this); ?>
<?php ThemeAsset::register($this); ?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?= Html::csrfMetaTags() ?>

    <title><?= Html::encode(strip_tags($this->title)) ?></title>

    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?php FlashMsg::widget(); ?>


<div id="dashboard">

    <?=  MenuCustom::widget(['type' => 'menu-bs4']) ?>
    <?/*=  Menu::widget([]) */?>

    <div id="wrapper">
        <div id="sidebar-default" class="main-sidebar">


            <?= UserMenuAdmin::widget() ?>

            <?= MenuAdmin::widget() ?>

            <?= UserHistory::widget() ?>

            <div class="bottom-menu hidden-sm">
                <ul>
                    <li><a href="#"><i class="ion-help"></i></a></li>
                    <li>
                        <a href="#">
                            <i class="ion-archive"></i>
                            <span class="flag"></span>
                        </a>
                        <ul class="menu">
                            <li><a href="#">5 unread messages</a></li>
                            <li><a href="#">12 tasks completed</a></li>
                            <!-- <li><a href="#">3 features added</a></li> -->
                        </ul>
                    </li>
                    <li>
                        <a href="<?= Url::toRoute('/users/user/logout') ?>" data-method="POST">
                            <i class="ion-log-out"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div id="content">

            <?= Breadcrumbs::widget(['items' => $this->params['breadcrumbs']]) ?>

            <div class="menubar">
                <div class="sidebar-toggler visible-xs">
                    <i class="ion-navicon"></i>
                </div>

                <h1 class="page-title">
                    <?= $this->title ?>
                </h1>

                <div class="second-menu hidden-xs">
                    <?= isset($this->params['secondMenu']) ? $this->params['secondMenu'] : '' ?>
                </div>
            </div>
            <?= AjaxProgressLine::widget() ?>

            <div class="content-wrapper">

                <?= $content ?>

            </div>
        </div>
    </div>

    <?= SkinSwitcherAdmin::widget() ?>
</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
