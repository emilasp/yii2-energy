<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>"/>
        <style type="text/css">
            .heading {
            }

            .list {
            }

            .footer {
            }
        </style>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>


    <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td align="">

                <h1 style="color: #000000"><?= Yii::$app->params['sitename'] ?? '' ?></h1>

                <table width="540" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
                    <tr>
                        <td>
                            <?= $content ?>
                        </td>
                    </tr>
                </table>


                <hr />

                <table width="660" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
                    <tr>
                        <td style="padding: 10px" align="left">
                            <a href="<?= Url::to(['/'], true) ?>">
                              <!-- <strong><?/*= Yii::$app->params['sitename'] */?></strong>-->
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>


    <?php $this->endBody() ?>

    </body>
    </html>
<?php $this->endPage() ?>