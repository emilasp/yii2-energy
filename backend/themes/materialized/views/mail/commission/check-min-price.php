Найдена новая минимальная цена <strong><?= $model->catalog->name ?></strong> <time><?= date('d.m.Y') ?></time> <br /><br />

<strong>Наша цена:</strong> <?= \yii\helpers\Html::a(
    Yii::$app->formatter->asCurrency($model->price_our),
    \emilasp\commission\common\models\ImCheckPrice::getMonetnikUrl($model->im_id),
    ['data-pjax' => 0, 'target' => '_blank']
) ?>
<strong>Новая цена:</strong>  <?= \yii\helpers\Html::a(
    Yii::$app->formatter->asCurrency($model->price_min),
    \emilasp\commission\common\models\ImCheckPrice::getMonetnikUrl($model->price_min_im_id ?: ''),
    ['data-pjax' => 0, 'target' => '_blank']
) ?>
