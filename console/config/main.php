<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id'                  => 'app-console',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log', 'cms'],
    'controllerNamespace' => 'console\controllers',
    'components'          => [
        'log' => [
            'targets' => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'urlManager' => require(__DIR__ . '/../../frontend/config/include/urlManager.php'),

        'parser' => [
            'class' => \emilasp\parser\components\ParserComponent::class,

            'stages' => [
                'page_urls' => [
                    'type'    => \emilasp\parser\components\ParserComponent::TYPE_URL_ITERATOR,
                    'options' => [
                        'max'      => 9,
                        'template' => 'href="http://prosud24.ru/zhilishhnoe-pravo/page/{ITERATOR}/',
                    ]
                ],
                'item_urls' => [
                    'type'    => \emilasp\parser\components\ParserComponent::TYPE_URL_REGEXP,
                    'options' => [
                        'template' => '.news-title-h2',
                    ],
                ],
                'item'      => [
                    'type'       => \emilasp\parser\components\ParserComponent::TYPE_ITEM,
                    'options'    => [],
                    'attributes' => [
                        'title' => [],
                        'h1'    => [],
                    ]
                ]
            ]
        ]
    ],

    'modules' => [
        'cms'        => ['class' => 'emilasp\cms\console\CmsModule'],
        'seo'        => ['class' => 'emilasp\seo\SeoConsoleModule'],
        'parser'     => ['class' => 'emilasp\parser\ParserModule'],
        'commission' => ['class' => 'emilasp\commission\console\CommissionModule'],
    ],


    'controllerMap' => [
        'migrate' => [
            'class'   => 'emilasp\core\commands\MigrateController',
            'default' => [
                'db'     => 'db',
                'module' => 'app',
            ],
            'modules' => [
                'app'        => '@console/migrations',
                'users'      => '@vendor/emilasp/yii2-user-x/migrations',
                'variety'    => '@vendor/emilasp/yii2-variety/migrations',
                'settings'   => '@vendor/emilasp/yii2-settings/migrations',
                'json'       => '@vendor/emilasp/yii2-json/migrations',
                'media'      => '@vendor/emilasp/yii2-media/migrations',
                'taxonomy'   => '@vendor/emilasp/yii2-taxonomy-x/migrations',
                'seo'        => '@vendor/emilasp/yii2-seo-x/migrations',
                'site'       => '@vendor/emilasp/yii2-site-x/migrations',
                'social'     => '@vendor/emilasp/yii2-social/migrations',
                'cms'        => '@vendor/emilasp/yii2-cms/migrations',
                'commission' => '@vendor/emilasp/yii2-commission/migrations',
            ],
        ],
    ],


    'params' => $params,
];
